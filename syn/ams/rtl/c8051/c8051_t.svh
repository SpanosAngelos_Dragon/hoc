package c8051_t;

`include "c8051/ISA/operator.svh"

typedef struct packed {
   logic [log2(GROUP_N)  -1 : 0] group;
   logic [log2(MEMBER_N) -1 : 0] member;
} operator_t;

endpackage
