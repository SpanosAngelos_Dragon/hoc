// ===========================================================================
//  File          :    RegisterBank.sv
//  Entity        :    RegisterBank
//  Purpose       :    Execute Register Bank.
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 06-Oct-2013
// ===========================================================================
module RegisterBank # (
   parameter REGISTER_N = 8,
   parameter REGISTER_W = 8
) (
   input clkP,
   input aresetNP,
   input [Math::log2(REGISTER_N) -1 : 0] addrP,
   input wrP,
   input [REGISTER_W -1 : 0] registerValueInP,
   output [REGISTER_W  -1 : 0] registerValueOutP
);

reg [REGISTER_W -1 : 0] R [REGISTER_N -1 : 0];
reg registerValueOutW;

always @ (posedge clkP or negedge aresetNP)
   if (!aresetNP)
      for (int registerIndex = 0; registerIndex < REGISTER_N; registerIndex++ )
         R[registerIndex] = 0;
   else if ( wrP )
      for (int registerIndex = 0; registerIndex < REGISTER_N; registerIndex++ )
         R[registerIndex] = registerValueInP;

always @ *
   for (int registerIndex = 0; registerIndex < REGISTER_N; registerIndex++ )
      if (registerIndex == addrP )
         registerValueOutW = R[registerIndex];

assign registerValueOutP = registerValueOutW;
endmodule
