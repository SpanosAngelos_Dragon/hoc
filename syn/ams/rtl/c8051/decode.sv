// ===========================================================================
//  File          :    decode.sv
//  Entity        :    decode
//  Purpose       :    c8051 Decoder.
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 29-Sep-2013
//  TODO:
//    Add assertions.
//    Add missing operations.
// ===========================================================================
import math::log2;

module decode (
   input [`ISA_INSTRUCTION_W -1 : 0] instructionP,
   input                             instructionValidP,
   output                            instructionEnableP,

   output       basicValidP,
   output [3:0] basicOperationP,

   output       aPCValidP,
   output [3:0] aPCOperationP,

   output       lPCValidP,
   output       lPCOperationP,

   output       rotateValidP,
   output [1:0] rotateOperationP,

   output       jumpValidP,
   output [3:0] jumpOperationP,

   output       NOPValidP,

   output       retValidP,
   output       retOperationP,

   output       bitCValidP,
   output [1:0] bitCOperationP,

   output       bitValidP,
   output       bitOperationP,

   output       cValidP,
   output       cOperationP,

   output       aValidP,
   output [1:0] aOperationP,

   output       movxValidP,
   output [2:0] movxOperationP,

   output       stackValidP,
   output       stackOperationP,

   output       movcValidP,
   output       movcOperationP,

   output       cBitValidP,
   output       cBitOperationP,

   output       extraValidP,
   output       extraOperationP,

   output [`ISA_OPERAND_GROUP_MEMBER_W     -1 : 0 ] operandP,
   output                                           operandOutValidP,
   output [`ISA_OPERAND_W * `ISA_OPERAND_N -1 : 0 ] operandOutValueP

   input enableInP
);

wire operandOutEnableW;
wire operationEnableW;

operand_0 operand (
   .operationP          (instructionP[`ISA_OPERATION_W -1 : 0]),
   .operationInValidP   (instructionValidP),
   .operationOutEnableP (operationOutEnableW),

   .operandP            (operandP),
   .operandOutValidP    (operandOutValidP),
   .operandInEnableP    (enableInP)
);

assign operandOutValueP = instructionP[$size(instructionP) -1 : `ISA_OPERATION_W];

operation_0 operation (
   .operationP       (instruction[`ISA_OPERATION_W -1 : 0]),
   .operationValidP  (instructionValidP),
   .operationEnableP (operationEnableW),

   .basicValidP      (basicValidP),
   .basicOperationP  (basicOperationP),

   .aPCValidP        (aPCValidP),
   .aPCOperationP    (aPCOperationP),

   .lPCValidP        (lPCValidP),
   .lPCOperationP    (lPCOperationP),

   .rotateValidP     (rotateValidP),
   .rotateOperationP (rotateOperationP),

   .jumpValidP       (jumpValidP),
   .jumpOperationP   (jumpOperationP),

   .NOPValidP        (NOPValidP),

   .retValidP        (retValidP),
   .retOperationP    (retOperationP),

   .bitCValidP       (bitCValidP),
   .bitCOperationP   (bitCOperationP),

   .bitValidP        (bitValidP),
   .bitOperationP    (bitOperationP),

   .cValidP          (cValidP),
   .cOperationP      (cOperationP),

   .aValidP          (aValidP),
   .aOperationP      (aOperationP),

   .movxValidP       (movxValidP),
   .movxOperationP   (movxOperationP),

   .stackValidP      (stackValidP),
   .stackOperationP  (stackOperationP),

   .movcValidP       (movcValidP),
   .movcOperationP   (movcOperationP),

   .cBitValidP       (cBitValidP),
   .cBitOperationP   (cBitOperationP),

   .extraValidP      (extraValidP),
   .extraOperationP  (extraOperationP),

   .operand0TypeP    (operand0TypeP),
   .operandOutValueP (operandOutValueP),
   .operandOutValidP (operandOutValidP),

   .enableInP        (enableInP)
);

endmodule
