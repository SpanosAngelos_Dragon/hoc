// ===========================================================================
//  File          :    operand.sv
//  Entity        :    operand
//  Purpose       :    Operand Decode.
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 05-Oct-2013
// ===========================================================================
module operand (
   input  [ `ISA_OPERATION_W  -1 : 0 ] operationP,
   input                               operationInValidP,
   output                              operationOutEnableP,

   output [`ISA_OPERAND_GROUP_MEMBER_W -1 : 0 ] operandP,
   output                                       operandOutValidP,
   input                                        operandInEnableP
);

   assign operandOutValidP    = operationInValidP;
   assign operationOutEnableP = operandInEnableP;

   assign operandP = operation[`ISA_OPERAND_GROUP_MEMBER_W -1 : 0];

endmodule
