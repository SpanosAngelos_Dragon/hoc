/*! 
 * \file      XMLSectionTitleAttribute.cpp
 * \brief     ipXact
 * \details   XMLSectionTitleAttribute class
 * \author    Angelos Spanos
 * \version   1.0
 * \date      13-Jul-2013
 * \copyright GNU Public License.
 */

#include <string>
#include <sstream>

class XMLSectionTitleAttribute {
   public :
      string name  = "";
      string value = "";

      XMLSectionTitleAttribute(stringstream & identifier);

      stringstream getAttribute ();
   private :
      XMLSectionTitleAttribute * next = NULL;
};

XMLSectionTitleAttribute::XMLSectionTitleAttribute(stringstream & identifier) {

   if ( sscanf ( identifier, "%s=\"%s\"", this.name, this.value ) != 2) {
      cerr << "Attribute " << identifier << "didn't follow name::\"value\" convention." << endl;
      this.name = "Invalid.";
      this.value = "Invalid.";
      
   }

   if ( DEBUG__SWITCH_XMLSectionTitleIDentifier ) {
      cout << "XMLSectionTitleAttribute::identifier = " << identifier << endl;
      cout << "XMLSectionTitleAttribute::Name = "  << this.name  << endl;
      cout << "XMLSectionTitleAttribute::Value = " << this.value << endl;
   }

   if (identifier != "")
      next = new XMLSectionTitleAttribute(identifier);

   return;
}

string XMLSectionTitleAttribute::getAttribute() {
   stringstream identifier << this.name << "=" << '"' << this.value << '"';

   if (next != NULL)
      identifier << " " << next.getAttribute();

   return identifier.str( );
}
