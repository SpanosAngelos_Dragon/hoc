// ===========================================================================
//  File          :    program.sv
//  Entity        :    program
//  Purpose       :    Program RAM for the C8051.
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 29-Sep-2013
// ===========================================================================
//-----------------------------------------------------------------------------

import math::log2;

module program #(
   parameter DATA_W = 24,
   parameter DATA_N = 2^10,

   localparam ADDR_W = log2(DATA_N),
) (
   input                  clkP;
   input                  aresetNP;

   input  [ADDR_W -1 : 0] addrP,
   input  [DATA_W -1 : 0] dataInP,
   input                  validInP,
   output                 enableOutP,

   output [DATA_W -1 : 0] dataOutP,
   output                 validOutP,
   input                  enableInP
);

programRAM singlePortRAM #(
   .DATA_W  (DATA_W),
   .DATA_N  (DATA_N)
) (
   .clkP       (clkP),
   .aresetNP   (aresetNP),
   .addrP      (addrP),
   .dataInP    (dataInP),
   .dataOutP   (programRAMDataP),
   .rdNWr      (progNP & validInP)
);

assign enableOutP = progNP;

reg validOutR;
always @ (negedge aresetNP or posedge clk)
   if (!aresetNP)
      validOutR <= 1'b0;
   else
      validOutR <= progNP;
assign validOutP = validOutR;

endmodule
