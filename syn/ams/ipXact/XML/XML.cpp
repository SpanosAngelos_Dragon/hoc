/*! 
 * \file      XML.cpp
 * \brief     ipXact
 * \details   XML Class
 * \author    Angelos Spanos
 * \version   1.0
 * \date      11-Jul-2013
 * \copyright GNU Public License.
 */

#include <ifstream>
#include <iostream>
#include <string>

using std::cerr;
using std::getline;


class XML {
   protected :
      int status;                ///< Error status
   private : 
      XMLSection *rootSection;   ///< XML Section
   public :
      XML(ifstream xmlFile_if);  ///< XML constructor.
};

XML::XML( ifstream xmlFile_if ) {
   if ( xmlFile_if.bad() ) {
      cerr << "XML::xmlFile_if invalid." << endl;
      this.status = ERR__IO__BAD_IF;
      return;
   }

   while ( xmlFile_if.good() ) {
      string sectionTitle;
      getline ( xmlFile_if, sectionTitle, '>' );
      if ( DEBUG__SWITCH_XML ) {cout << "XML Title = " << sectionTitle << end;}
   }
}
