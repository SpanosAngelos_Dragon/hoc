/*! 
 * \file      XMLSection.cpp
 * \brief     ipXact
 * \details   XMLSection Class
 * \author    Angelos Spanos
 * \version   1.0
 * \date      13-Jul-2013
 * \copyright GNU Public License.
 */

#include <string>
#include <sstream>

using std::size_t;
using std::string;

class XMLSection {
   private : 
      XMLSectionTitle title;
      void          * content;
      stringstream    getSubSection();
   public :
      XMLSection( string & section );
      XMLSection * next;
};

XMLSection::XMLSection( string & section ) {
   stringstream section = new stringstream( section );

   string sectionTitle;
   if ( fscanf ( section, "<%s\\>", sectionTitle ) ) {
      XMLSectionTitle = new XMLSectionTitle( sectionTitle );
      content = NULL;
    }
    else if ( fscanf ( section, "<%s>", sectionTitle ) ) {
      XMLSectionTitle = new XMLSectionTitle( sectionTitle );
      content = new XMLSection ( getSubSection ( section ) );
    }

    if ( section == "" ) {
      next = new XMLSection ( section );
    }

    return;
}

stringstream getSubSection( stringstream & section ) { //?? This function doesn't not progress the section pointer.
   string subSection = section.str();

   string subSectionStartTag = string( "<" ) + title.name + string( ">" );
   string subSectionEndTag = string( "</" ) + title.name + string( ">" );

   size_t subSectionEndTagPos = subSection.find( subSectionEndTag);

   return stringstream( subSection.substr( subSectionStartTag.length( ), subSection.length() - subSectionEndTagPos) );
}
