// ===========================================================================
//  File          :    operation.sv
//  Entity        :    operation
//  Purpose       :    Operation Decode.
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 05-Oct-2013
// ===========================================================================
import math::log2;

module operation (
   input  [`OPERATION_W -1 :0] operationP,
   input                       operationValidP,
   output                      operationEnableP,
                             
   output                      basicValidP,
   output [            3 : 0 ] basicOperationP,
                             
   output                      aPCValidP,
   output [            3 : 0 ] aPCOperationP,
                             
   output                      lPCValidP,
   output                      lPCOperationP,
                             
   output                      rotateValidP,
   output [            1 : 0 ] rotateOperationP,
                             
   output                      jumpValidP,
   output [            3 : 0 ] jumpOperationP,
                             
   output                      NOPValidP,
                              
   output                      retValidP,
   output                      retOperationP,
                             
   output                      bitCValidP,
   output [            1 : 0 ] bitCOperationP,
                             
   output                      bitValidP,
   output                      bitOperationP,
                              
   output                      cValidP,
   output                      cOperationP,
                             
   output                      aValidP,
   output [            1 : 0 ] aOperationP,
                             
   output                      movxValidP,
   output [            2 : 0 ] movxOperationP, 
                             
   output                      stackValidP,
   output                      stackOperationP,
                              
   output                      movcValidP,
   output                      movcOperationP,
                              
   output                      cBitValidP,
   output                      cBitOperationP,
                              
   output                      extraValidP,
   output                      extraOperationP,

   input                       enableInP
);

wire [7 : 0] operation;
assign operation = operationP [$size(operationP) -: log2(`OPCODE_N)];

assign basicValidP     = (|operation[3:2]) & operationValidP;
assign basicOperationP = operation[(`basicValidP + 1)7:4];

assign aPCValidP = (operation[3:0] == 4'h1) & operationValidP;
assign aPCOperationP = operation[7:4];

assign lPCValidP   = (operation[3:0] == 4'h2) && (operation[7:5] == 0) & operationValidP;
assign lPCOperationP = (operation[0]);

assign rotateValidP = (operation[7:6] == 2'h0) && (operation[3:0] == 4'h3) & operationValidP;
assign rotateOperationP = operation[5:4];

assign jumpValidP = (operation[3:0] == 'h0) && (|operation[4:1] == 1'b1) & operationValidP;
assign jumpOperationP = operation[4:1];

assign NOPValidP =(operation[7:0] == 8'h0) & operationValidP;

assign retValidP = (operation[7:5] == 2'b001) && (operation[3:0] == 4'h2) & operationValidP;
assign retOperationP = operation[4];

assign bitCValidP = (operation[[7:4] >= 4'h7 && operation[[7:4] <= 4'hA) & (operation[3:0] == 4'h2) & operationValidP;
assign bitCOperationP = operation[7:4];

assign bitValidP = ((operation[7:4] == 'hC) | (operation[7:4] == 'hD)) & (operation[3:0] == 'h2) & operationValidP;
assign bitOperationP = operation[6];

assign cValidP = ((operation[7:4] == 'hC) | (operation[7:4] == 'hD)) & (operation[3:0] == 'h3) & operationValidP;
assign cOperationP = operation[6];

assign aValidP = (operation[7:6] == 2'b11) & (operation[3:0] == 4'h4) & operationValidP;
assign aOperationP = operation[5:4];

assign movxValidP   = (operation[3:2] == 2'b00) & (operation[1:0] != 2) & (operation[7:5] == 3'b111) & operationValidP;
assign movxOperationP = {operation[4], operation[1:0]};

assign stackValidP = ((operation[7:4] == 'hC) | (operation[7:4] == 'hD)) & (operation[3:0] == 4'h4) & operationValidP;
assign stackOperationP = operation[4];

assign movcValidP = ((operation[7] == 1'b1) & operation[6:5] == 2'b00) & (operation[3:0] == 4'h3) & operationValidP;
assign movcOperationP = operation[4];

assign cBitValidP = (operation[7:5] == 3'b101) & (operation[3:0] == 0) & operationValidP;
assign cBitOperationP = (operation[4];

assign extraValidP = (operation[7:4] == 4'b10x0) & (operation[3:0] == 'h4) & operationValidP;
assign extraOperationP = (operation[5];

assign operationEnableP = enableInP;

endmodule
