// ===========================================================================
//  File          :    stack.svh
//  Entity        :    stack
//  Purpose       :    Stack Operations
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 06-Oct-2013
// ===========================================================================
`define STACK_OP_NONE 0
`define STACK_OP_WR   (`STACK_OP_NONE + 1)
`define STACK_OP_RD   (`STACK_OP_WR   + 1)
`define STACK_OP_N    (`STACK_OP_RD   + 1)

`define STACK_MAX_BYTE 2
