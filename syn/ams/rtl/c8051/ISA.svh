// ===========================================================================
//  File          :    ISA.svh
//  Entity        :    ISA 
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 06-Jul-2013
// ===========================================================================
`include "ISA/opcode.svh"
`include "ISA/operation.svh"
`include "ISA/operand.svh"
`include "ISA/operandNum.svh"
`define ISA_INSTRUCTION_W (`ISA_OPERATION_W + `ISA_OPERAND_W)
