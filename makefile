# ===========================================================================
#  File          :    makefile
#  Purpose       :    Hospital On Chip project makefile.
# ===========================================================================
#  Design Engineer : Angelos Spanos (TE)
#  Creation Date   : 04-Jul-2013
# ===========================================================================

root := $(shell pwd)
export root

include makefile.node.rules
