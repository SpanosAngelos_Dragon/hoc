// ===========================================================================
//  File          :    execute_t.svh
//  Entity        :    execute_t
//  Purpose       :    Types for the execute unit.
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 05-Oct-2013
// ===========================================================================

typedef struct packed {
   logic [7:0] bitIndex;
   logic [7:0] offset;
} bitOffset_t;

typedef struct packed {
   logic [7:0] direct;
   logic [7:0] immed;
} directImmed_t;
