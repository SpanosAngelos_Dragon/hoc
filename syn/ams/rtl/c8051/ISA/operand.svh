// ===========================================================================
//  File          :    operand.svh
//  Entity        :    operand
//  Purpose       :    ISA Operand
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 04-Oct-2013
// ===========================================================================
`define OPERAND_C_BIT            0
`define OPERAND_DIRECT_A         (`OPERAND_C_BIT        + 2)
`define OPERAND_DIRECT_IMMED     (`OPERAND_DIRECT_A     + 1)
`define OPERAND_IMMED_A          (`OPERAND_DIRECT_IMMED + 1)
`define OPERAND_DIRECT           (`OPERAND_IMMED_A      + 1)
`define OPERAND_R0_ADDR          (`OEPRAND_DIRECT       + 1)
`define OPERAND_R1_ADDR          (`OPERAND_R0_ADDR      + 1)
`define OPERAND_R0               (`OPERAND_R1_ADDR      + 1)
`define OPERAND_R1               (`OPERAND_R0           + 1)
`define OPERAND_R2               (`OPERAND_R1           + 1)
`define OPERAND_R3               (`OPERAND_R2           + 1)
`define OPERAND_R4               (`OPERAND_R3           + 1)
`define OPERAND_R5               (`OPERAND_R4           + 1)
`define OPERAND_R6               (`OPERAND_R5           + 1)
`define OPERAND_R7               (`OPERAND_R6           + 1)

`define OPERAND_MOVX_DPTR        0
`define OPERAND_MOVX_RO_ADDR     (`OPERAND_MOVX_DPTR + 2)
`define OPERAND_MOVX_R1_ADDR     (`OPERAND_MOVX_DPTR + 1)
