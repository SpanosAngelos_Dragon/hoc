/*! 
 * \file      XMLSectionTitle.cpp
 * \brief     ipXact
 * \details   XMLSectionTitle Class
 * \author    Angelos Spanos
 * \version   1.0
 * \date      13-Jul-2013
 * \copyright GNU Public License.
 */

#include <iostream>
#include <string>
#include <sstream>

using namespace std;

class XMLSectionTitle {
   private :
      XMLSectionTitleIdentifier * rootIdentifier = NULL;
   public :
      string name;
      XMLSectionTitle(stringstream & sectionTitle);
      stringstream getTitle;
};

XMLSectionTitle::XMLSectionTitle(stringstream & sectionTitle) {
   sectionTitle >> this.name;
   
   if (sectionIdentifier != "")
      rootIdentifier = new XMLSectionTitleIdentifier(sectionTitle);

   if (DEBUG__SWITCH_XMLSectionTitle) {
      cout << "XMLSectionTitle::sectionTitle = " << sectionTitle;
      cout << "XMLSectionTitle::title        = " << this.title;
      cout << "XMLSectionTitle::identifier   = " << this.rootIdentifier.getIdentifier();
   }

   return;
}

stringstream getTitle() {
   stringstream title << this.name;
   if ( rootIdentifier != NULL )
      title << rootIdentifier.getIdentifier();

    return title;
}
